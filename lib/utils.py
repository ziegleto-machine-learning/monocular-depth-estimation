import torchvision.transforms as T
from lib.data.diode import DiodeDataset
from lib.data.nyu import NYUv2Dataset
from torch.utils.data import DataLoader
from tqdm import tqdm
import time
import torch
import numpy as np
import matplotlib.pyplot as plt


def get_nyu_dataloaders(path, batch_size=32):
    """ Return Dataloaders for training """
    # Transforms
    transforms = T.Compose([
        T.ToPILImage(),
        T.Resize(256, antialias=True),
        T.CenterCrop(224),
        T.ToTensor(),
        T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    target_transforms = T.Compose([
        T.Resize(256, antialias=True),
        T.CenterCrop(224),
    ])
    # Datasets
    full_dataset = NYUv2Dataset(
        path,
        transform=transforms,
        target_transform=target_transforms
    )
    train_size = int(0.8 * len(full_dataset))
    val_size = len(full_dataset) - train_size
    dataset_train, dataset_val = torch.utils.data.random_split(
        full_dataset, 
        [train_size, val_size]
    )
    # Dataloaders
    dataloader_train = DataLoader(
        dataset_train,
        batch_size=batch_size,
        shuffle=True
    )
    dataloader_val = DataLoader(
        dataset_val,
        batch_size=batch_size,
        shuffle=True
    )
    return (
        dataloader_train,
        dataloader_val
    )


def run_epoch(
    dataloader,
    optimizer,
    device,
    model,
    C,
    val=False
) -> None:
    t_start = time.time()
    if val:
        model.eval()
    losses = []
    for batch in tqdm(dataloader):
        X, Y = batch
        X, Y = X.to(device), Y.to(device)

        optimizer.zero_grad()
        with torch.set_grad_enabled(not val):
            Y_tild = model(X)
            loss = C(Y, Y_tild)
            if not val:
                loss.backward()
                optimizer.step()
            losses.append(loss.item())
    epoch_loss = np.mean(losses)
    exec_time = t_start - time.time()
    model.train()

    return epoch_loss, exec_time


def visualize_example(
    model,
    dataloader,
    i,
    device
) -> None:
    model.eval()
    with torch.no_grad():
        X, Y = next(iter(dataloader))
        X, Y = X.to(device), Y.to(device)
        x = X[0].unsqueeze(0)
        y = Y[0]
        pred = model(x).detach()
        y = y.permute(1, 2, 0)
        pred = pred[0].permute(1, 2, 0)

        fig, axs = plt.subplots(1, 2)
        axs[0].imshow(y)
        axs[0].set_title('GT')
        axs[1].imshow(pred)
        axs[1].set_title('Prediction')
        plt.savefig(f'outputs/{i}.jpg')
    model.train()
