import cv2
import torch
import matplotlib.pyplot as plt

class MidasEstimator:
    def __init__(self):
        self.model = torch.hub.load("intel-isl/MiDaS", "DPT_Large")
        midas_transforms = torch.hub.load("intel-isl/MiDaS", "transforms")
        self.transform = midas_transforms.dpt_transform

    def estimate(self, x, device):
        self.model.to(device)
        self.model.eval()
        input_batch = self.transform(x).to(device)
        with torch.no_grad():
            prediction = self.model(input_batch)

            prediction = torch.nn.functional.interpolate(
                prediction.unsqueeze(1),
                size=x.shape[:2],
                mode="bicubic",
                align_corners=False,
            ).squeeze()

        output = prediction.cpu().numpy()
        return output


def set_default(figsize=(15, 5), dpi=100):
    plt.style.use(['dark_background', 'bmh'])
    plt.rc('axes', facecolor='k')
    plt.rc('figure', facecolor='k')
    plt.rc('figure', figsize=figsize, dpi=dpi)

set_default()

def main():
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    estimator = MidasEstimator()

    img = cv2.imread('/Users/tobias/data/5Safe/peter_paul/images/DJI_0504.JPG_resized.JPG')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    output = estimator.estimate(img, device)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(img)
    ax2.imshow(output)
    ax1.axis('off')
    ax2.axis('off')
    plt.show()

if __name__ == '__main__':
    main()