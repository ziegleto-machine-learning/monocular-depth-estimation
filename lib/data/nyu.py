import h5py
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset
import torchvision.transforms as T
import torch

class NYUv2Dataset(Dataset):
    """ Loads whole dataset in ram. Should be faster, but keep size in mind. """ 
    def __init__(self, path, transform=None, target_transform=None):
        self.file = h5py.File(path, mode='r')
        self.color_maps = self.file['images']
        self.depth_maps = self.file['depths']
        self.transform = transform
        self.target_transform = target_transform

    def close(self):
        self.file.close()

    def __len__(self):
        return len(self.color_maps)

    def __getitem__(self, idx):
        color_map = self.color_maps[idx].T
        depth_map = self.depth_maps[idx].T

        if self.transform:
            color_map = self.transform(color_map)
        if self.target_transform:
            depth_map = torch.from_numpy(depth_map).unsqueeze(0) # C H W 
            depth_map = self.target_transform(depth_map)

        return color_map, depth_map


def plot_img_and_depth(x, y):
    x, y = x.permute(1, 2, 0), y.permute(1, 2, 0)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(x)
    ax2.imshow(y, cmap='Spectral')
    plt.show() 
    
def main():
    transforms = T.Compose([
        T.ToPILImage(),
        T.Resize(256, antialias=True),
        T.CenterCrop(224),
        T.ToTensor(),
        T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    target_transforms = T.Compose([
        T.Resize(256, antialias=True), 
        T.CenterCrop(224),
    ])

    dataset = NYUv2Dataset(
        '/Users/tobias/data/monocular_depth_estimation/nyu/nyu_depth_v2_labeled.mat',
        transform=transforms,
        target_transform=target_transforms
    )
    i = np.random.randint(0, len(dataset))
    x, y = dataset.__getitem__(i)
    dataset.close()

    plot_img_and_depth(x, y)

if __name__ == '__main__':
    main()