# Monocular Depth Estimation using Dinov2

## Baseline
To have a baseline to compare with, we have a midas depth estimator and a
randomly guessing estimator.

## Dinov2
Dinov2 is a transformer backbone trained on many images. The authors claim
that they can use the backbone for various tasks. In this repo, we want
to try Dinov2 as a backbone and try it out on several datasets, e.g., 
NYUv2. 

We want to also try a very simple approach with just a linear header, but
up until now not working.

For this we will provide several flavours of heads for the backbone. The 
first one is inspired by ranftl et al. However, we only use the last output
of the backbone first. 

A more sophisticated approach will also use intermediate outputs of the 
transformer blocks.

We will also test binning vs non-binning.

## Loss Function
Currently, we are only using `MSE` from torch. But we want to test out, 
how we can approach this in a better way. We think, that a pixel-based loss
is not the way to go for any image-related task. We think we need a 
latent-based loss function, weighted with a output-based function like `MSE`.
Until now, we have not implemented it, as we first need to implement the
basic functionality with the `MSE` function.

## Dataset and preprocessing
To begin with we only use NYUv2. 

### NYUv2
We have no preprocessing until now. You can find the implementation of the
dataset in the `lib.data.nyu.py` file.
We will insert some images about the data characteristics, e.g., histograms, 
etc., later.

## Results
To be done. (Table)
