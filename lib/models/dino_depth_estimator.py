import torch
import torch.nn as nn


class Bin(torch.nn.Module):
    def __init__(self, num_bins=256, _min=0, _max=100):
        super(Bin, self).__init__()
        self.bin_edges = torch.linspace(_min, _max, num_bins+1)

    def forward(self, x):
        values = x.flatten()
        binned_values = torch.bucketize(values, self.bin_edges)
        _x = binned_values.reshape(x.shape)
        x = _x.to(x.dtype)
        return x


class Transpose(nn.Module):
    def __init__(self, dim0, dim1):
        super(Transpose, self).__init__()
        self.dim0 = dim0
        self.dim1 = dim1

    def forward(self, x):
        x = x.transpose(self.dim0, self.dim1)
        return x


class DinoDepthEstimator(torch.nn.Module):
    def __init__(
            self,
            backbone_output_dim=1024,
            D_hat_dim=256,
            patch_size=14,
            img_height=224,
            img_width=224
    ) -> None:
        super(DinoDepthEstimator, self).__init__()

        self.backbone = torch.hub.load(
            'facebookresearch/dinov2', 'dinov2_vitl14')
        for p in self.backbone.parameters():
            p.requires_grad = False

        self.head = nn.Sequential(
            # 1. Read Projection (Np+1 x D --> Np x D)
            nn.Linear(
                backbone_output_dim*2,
                backbone_output_dim
            ),
            nn.GELU(),
            Transpose(1, 2),
            # 2. Patchify again (Np x D --> H/p x W/p x Dhat)
            nn.Unflatten(
                2,
                torch.Size([
                    int(img_width/patch_size),
                    int(img_height/patch_size)
                ])
            ),
            nn.Conv2d(
                in_channels=backbone_output_dim,
                out_channels=D_hat_dim,
                kernel_size=1,
                stride=1,
                padding=0
            ),
            # 3. Pixelfy (H/p x W/p x Dhat --> H x W x Dhat)
            nn.ConvTranspose2d(
                in_channels=D_hat_dim,
                out_channels=D_hat_dim,
                kernel_size=patch_size,
                stride=patch_size,
                padding=0,
                bias=True,
                dilation=1,
                groups=1
            ),
            # 4. Predictify (H x W x Dhat --> H x W x 1)
            nn.Conv2d(
                in_channels=D_hat_dim,
                out_channels=1,
                kernel_size=1,
                stride=1,
                padding=0
            )
        )

    def forward(self, x):
        # Get Embeddings from Backbone
        with torch.no_grad():
            embeddings = self.backbone.forward_features(x)
            clstoken = embeddings['x_norm_clstoken']
            patchtokens = embeddings['x_norm_patchtokens']
            clstoken_expanded = clstoken \
                .unsqueeze(1) \
                .expand(-1, patchtokens.size(1), -1)
            concatenated_tokens = torch.cat(
                [clstoken_expanded, patchtokens],
                dim=2
            )
            x = concatenated_tokens
        x = self.head(x)
        return x
